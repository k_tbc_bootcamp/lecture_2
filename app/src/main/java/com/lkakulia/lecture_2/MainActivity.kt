package com.lkakulia.lecture_2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btn.setOnClickListener({
            val randomNumber = randomNumberGenerator()

            when (randomNumber) {
                true -> toastMessagePopUp("even")
                false -> toastMessagePopUp("odd")
            }
        })
    }

    fun randomNumberGenerator(): Boolean {
        val randomNumber = (1..101).random()

        return randomNumber % 2 == 0
    }

    fun toastMessagePopUp(message: String) {
        Toast.makeText(this, "The number is $message", Toast.LENGTH_SHORT).show()
    }
}
